document.addEventListener('DOMContentLoaded', () => {
  const taskForm = document.getElementById('task-form');
  const taskInput = document.getElementById('task-input');
  const taskList = document.getElementById('task-list');

  // Load tasks from local storage
  loadTasks();

  // Add task
  taskForm.addEventListener('submit', (e) => {
    e.preventDefault();
    if (taskInput.value === '') return;
    addTask(taskInput.value);
    taskInput.value = '';
  });

  // Delete task
  taskList.addEventListener('click', (e) => {
    if (e.target.classList.contains('delete')) {
      if (confirm('Are you sure?')) {
        e.target.parentElement.remove();
        removeTaskFromLocalStorage(e.target.parentElement);
      }
    }
  });

  function addTask(task) {
    // Create li element
    const li = document.createElement('li');
    li.className = 'mb-4 border p-2 rounded';
    li.appendChild(document.createTextNode(task));

    // Create delete button
    const deleteBtn = document.createElement('button');
    deleteBtn.className = 'delete ml-2 bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded';
    deleteBtn.innerHTML = '<i class="fas fa-trash-alt"></i>';
    li.appendChild(deleteBtn);

    // Append li to ul
    taskList.appendChild(li);

    // Store in local storage
    storeTaskInLocalStorage(task);
  }

  function loadTasks() {
    let tasks;
    if (localStorage.getItem('tasks') === null) {
      tasks = [];
    } else {
      tasks = JSON.parse(localStorage.getItem('tasks'));
    }

    tasks.forEach((task) => {
      addTask(task);
    });
  }

  function storeTaskInLocalStorage(task) {
    let tasks;
    if (localStorage.getItem('tasks') === null) {
      tasks = [];
    } else {
      tasks = JSON.parse(localStorage.getItem('tasks'));
    }

    tasks.push(task);
    localStorage.setItem('tasks', JSON.stringify(tasks));
  }

  function removeTaskFromLocalStorage(taskItem) {
    let tasks;
    if (localStorage.getItem('tasks') === null) {
      tasks = [];
    } else {
      tasks = JSON.parse(localStorage.getItem('tasks'));
    }

    tasks.splice(tasks.indexOf(taskItem.textContent), 1);
    localStorage.setItem('tasks', JSON.stringify(tasks));
  }
});
